package it.unibo.oop.lab.enum1;

import it.unibo.oop.lab.socialnetwork.User;

/**
 * This is going to act as a test for
 * {@link it.unibo.oop.lab.enum1.SportSocialNetworkUserImpl}
 * 
 * 1) Realize the same test as the previous exercise, this time using
 * enumeration Sport
 * 
 * 2) Run it: every test must return true.
 * 
 */
public final class TestSportByEnumeration {

    private TestSportByEnumeration() {
    }

    /**
     * @param args
     *            ignored
     */
    public static void main(final String... args) {
        /*
         * [TEST DEFINITION]
         * 
         * By now, you know how to do it
         */
        // TODO
    	SportSocialNetworkUserImpl mario = new SportSocialNetworkUserImpl<>("Mario", "Rossi", "mario01", 46);
    	
    	mario.addSport(Sport.F1);
    	System.out.println(mario.hasSport(Sport.MOTOGP));
    	System.out.println(mario.hasSport(Sport.F1));
    }

}
