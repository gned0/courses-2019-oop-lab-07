package it.unibo.oop.lab.nesting2;
import java.util.List;
import java.util.Iterator;

public class OneListAcceptable<T> implements Acceptable<T>{

	private final List<T> list;
	
	public OneListAcceptable(final List<T> list) {
		this.list = list;
	}
	
	@Override
	public Acceptor<T> acceptor() {
		Iterator<T> iterator = list.iterator();
		return new Acceptor<T>(){

			@Override
			public void accept(T newElement) throws ElementNotAcceptedException {
				// TODO Auto-generated method stub
				try {
					if(newElement.equals(iterator.next())) {
						return;
					}
				} catch (Exception e) {
					System.out.println("No more elements");
				}
				throw new ElementNotAcceptedException(newElement);
			}

			@Override
			public void end() throws EndNotAcceptedException {
				try {
					if(!iterator.hasNext()) {
						return;
					}
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
				throw new EndNotAcceptedException();
			}
			
			
		};
	}

}
